import { createApp } from 'vue'
import App from './App.vue'
// import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret, faCartShopping, faCar, faXmark, faBicycle, faEye, faHand, faHashtag, faCircleXmark } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// import { faComment } from '@fortawesome/free-regular-svg-icons'
// import { faTwitter } from '@fortawesome/free-brands-svg-icons'


library.add(faUserSecret, faCartShopping, faCar, faXmark, faBicycle, faEye, faHand, faHashtag, faCircleXmark)
// Vue.component('font-awesome-icon', FontAwesomeIcon)


createApp(App)
.component('font-awesome-icon', FontAwesomeIcon)
.mount('#app')