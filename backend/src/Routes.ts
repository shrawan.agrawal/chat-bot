import axios from "axios";
import { Text, send } from "./lib/BotResponse";
import { handleIncomingMessage } from "./lib/messageReceivedHandler";
import { handleRegister } from "./lib/registerHandler";
import { updateSchedule } from "./lib/updateHandler";
import { Register } from "./models/Register";
import express from 'express';
import { Subscriber } from "./models/Subscriber";
import { CustomButtons } from "./models/CustomButtons";
import { UserDetail } from "./models/UserDetails";
// ngrok http --domain=promoted-tahr-game.ngrok-free.app 8060
const app = express.Router()
app.get('/user_details', async (req, res) => {
  console.log("Details query")
  try {
      // const users = await Register.find({}, { _id: 0 });
      const users = await Register.aggregate([
        {
          $sort: { viberId: 1, created_at: -1 } // Sort by user_id and createdAt in descending order
        },
        {
          $group: {
            _id: '$viberId', // Group by user_id
            entries: { $push: '$$ROOT' } // Push each entry into an array
          }
        },
        {
          $project: {
            _id: 0,
            user_id: '$viberId',
            entries: { $slice: ['$entries', 2] } // Take up to 2 entries per user
          }
        }
      ]);
      
      const flattenedArray = users.reduce((accumulator, currentArray) => {
        return accumulator.concat(currentArray.entries);
      }, []);
      res.status(200).json(flattenedArray.sort((a,b)=>parseInt(b.created_at)-parseInt(a.created_at)));
    } catch (error) {
      res.status(500).json({ message: 'Error fetching user details' });
    }
});
  
  app.get("/", (req,res)=>{res.send("Healthy, Chat bot is working!!")})
    
  app.post('/register', async(req,res)=>{
    handleRegister(req, res);
  });
  
  app.put('/update', updateSchedule);
  
  app.post('/', async(req, res)=>{
    const event = req.body.event
    console.log("New event")

    if(event=='webhook'){
      console.log(event)
      res.status(200).send()
    }else if(event == "message"){
    console.log(event)
    console.log(req.body.message.text, req.body.sender.name)
    await handleIncomingMessage(req.body.message,req.body.sender)
    }else if(event == "conversation_started"){

      await send(req.body.user.id, `Hello ${req.body.user.name}, welcome to report bot! \nYour best friend to help you report your daily movement!`)
    }else if(event === "subscribed"){
      const subscriber = new Subscriber({ viberId: req.body.user.id, name:'', department:'', state:"name_input" });
      subscriber.save()
      await send(req.body.user.id, `Hello ${req.body.user.name}, welcome to report bot! \nYour best friend to help your report you daily movement!`)
      await send(req.body.user.id, "How should I call you, please enter your name.")
    }else if(event === "unsubscribed"){
      // console.log(req.body)
      const user = await Subscriber.findOne({viberId:req.body.user_id})
      await Subscriber.deleteOne({viberId:req.body.user_id})
      await CustomButtons.deleteOne({viberId:req.body.user_id})
      await UserDetail.deleteOne({viberId:req.body.user_id})
      await Register.deleteMany({viberId:req.body.user_id})
      await send(req.body.user_id, `Good Bye, ${user.name}!`)
    }
    // else if(event === "seen"){
    //   res.status(400).json()
    //   // res.send()
    // }else if(event === "delivered"){
    //   res.status(400).json()
    //   // res.send()
    // }
    else{
      res.status(400).json()
    }
    res.send()
})
export default app;