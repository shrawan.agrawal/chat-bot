import mongoose from "mongoose";

const subscrerSchema = new mongoose.Schema({
  viberId: { type: String, required: true, unique:true},
  name: { type: String},
  department: { type: String },
  // isAdmin:{type:Boolean, default:false},
  state: {
    type: String,
    enum: ['completed', 'name', 'name_input', 'department', 'department_input'], // Add more states as needed
    required: true
  }
});

export const Subscriber =  mongoose.model("Subscriber",subscrerSchema);
