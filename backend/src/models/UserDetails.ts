import mongoose from "mongoose";

const userDetailSchema = new mongoose.Schema({
    viberId: { type: String, required: true},
    name: String,
    deptChoice: String,
    fromChoice: String,
    toChoice: String,
    hourChoice: String,
    minuteChoice: String,
    userLate: Number,
    currentStage: String,
    nextExpectedInput: String,
    scheduledArrivalTimes: String,
    trip_time: String,
  });

export const UserDetail =  mongoose.model('UserDetail', userDetailSchema);
