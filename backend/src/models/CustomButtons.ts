import mongoose from "mongoose";

const customButtons = new mongoose.Schema({
    viberId: { type: String, required: true, unique:true },
    // chk
    // dept_list_rm: {type: Array, default: [ 'goss', 'IES', 'SAP', 'HR','R&D', 'OtherDept','Cancel']},
    from_list_rm: {type: Array, default: ['Home', 'RCH', 'From Other Loc','Cancel']},
    to_list_rm: {type: Array, default: ['Home', 'RCH', 'To Other Loc','Cancel']},
    minutes_list_rm:{type: Array, default: ['30', '60', '90', '120', '150', 'Cancel']}
  });
  
  

export const CustomButtons = mongoose.model('CustomButtons', customButtons);
