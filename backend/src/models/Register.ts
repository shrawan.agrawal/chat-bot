import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
  viberId: { type: String, required: true},
  name: { type: String, required: true },
  from: { type: String, required: true },
  to: { type: String, required: true },
  scheduled_arrival: { type: Number, required: true },
  trip_time: {type: String, required: true},
  actual_arrival: { type: Number, default: "" },
  status: { type: String, default: "" },
  department: { type: String, required: true },
  created_at: {type: Number, required: true},
  bot_state: {type: Number, default: 0},
  notified: {type:Boolean, default:false},
  waitingForReason: { type: Boolean, default: false },  
});

export const Register = mongoose.model('Register', userSchema);
Register.collection.createIndex({ created_at: 1 }, { expireAfterSeconds: 45 * 24 * 60 * 60 });
