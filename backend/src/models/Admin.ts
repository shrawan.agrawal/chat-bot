import mongoose from "mongoose";


const adminSchema = new mongoose.Schema({
    viberId: { type: Array, default: 'y6mWN1ILdijM8XgqVzeoHw==' }
});

export const Admin =  mongoose.model("Admin",adminSchema);
