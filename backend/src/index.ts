/* eslint-disable @typescript-eslint/no-unused-vars */

import dotenv from 'dotenv';
import express from 'express';
import ngrok from 'ngrok';
import bodyParser from 'body-parser'
import colors from 'colors'
import lt from "localtunnel"

// import { Bot as ViberBot, Events as BotEvents, Message } from 'viber-bot';
import { handleIncomingMessage } from './lib/messageReceivedHandler';
import { connect } from './Connect';
import { admin, adminUpdate } from './lib/Admin';
import botRoutes from './Routes';
import cors from "cors";
import axios, { AxiosRequestConfig } from 'axios'

dotenv.config();

// const TextMessage = Message.Text;

connect();

admin();

const app = express();
app.use(bodyParser.json())
app.set('view engine', 'ejs');
app.use(cors({origin:'*'}))

app.use("/", botRoutes)

setInterval(adminUpdate, 5 * 60 * 1000);

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Total-Count, X-Requested-With, Content-Type, Accept, authorization"
  );
  res.header("Access-Control-Expose-Headers", "X-Total-Count");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});


// const bot = new ViberBot({
//   authToken: process.env.VIBER_AUTH_TOKEN,
//   name: "ReportingViberBot",
//   avatar: "https://upload.wikimedia.org/wikipedia/commons/3/3d/Katze_weiss.png"
// });


// app.post('/', async(req, res)=>{
//   const event = req.body.event
//   if(event=='webhook'){
//     res.status(200).send()
//   }else if(event == "message"){
//     await handleIncomingMessage(req.body.message, req.body.sender.id)
//   }else if(event == "subscribed"){
//   }else{
//     res.status(400).json()
//   }
//   res.send()
// })
// app.use("/viber/webhook", bot.middleware());


// bot.on(BotEvents.MESSAGE_RECEIVED, async (message, response) => {await handleIncomingMessage(message, response);});


// bot.onConversationStarted((userProfile, isSubscribed, context, onFinish) =>
//   onFinish(new TextMessage(`Hi, ${userProfile.name}! Enter "register" to report your schedule `)));
// bot.onConversationStarted((userProfile, isSubscribed, context, onFinish) => {
//   const NameKeyboard = {
//     "Type": "keyboard",

//     "Buttons": [{

//       "Text": "Name",
//       "ActionType": "reply",
//       "ActionBody": "Name",
//       "BgMediaType": "picture",
//       "BgMedia": "https://i.imgur.com/PvQbZPi.png",
//       "BgLoop": true,
//       "ImageScaleType": "fit",
//       //"BgColor": "#F8f8f8",
//       // "BgColor": "#80FFFFFF",
//       "TextSize": "large",
//       "TextVAlign": "middle",
//       "TextHAlign": "center",
//       "TextOpacity": 100,

//     }]
//   };
//   onFinish(new TextMessage('hi !',NameKeyboard));
// });


const port: number = parseInt(process.env.PORT) || 8060;

const temFunc = async () => {
  // console.log("Hi")
  try{

  const url = await ngrok.connect({
    port:8060,
    // subdomain:"promoted-tahr-game.ngrok-free.app"
  });
  // const url2 = await ngrok.connect(8080);
  process.env.NGROK_URL = url
  console.log(url)
  // console.log(url2)
  const reqBody = {
    url,
    event_types: [
      // 'delivered',
      // 'seen',
      // 'failed',
      'subscribed',
      'unsubscribed',
      'conversation_started',
      'message',
    ],
    send_name: true,
    send_photo: true,
  }
  const response = await axios.post(`${process.env.viberApi}set_webhook`, reqBody, {
    headers:{
      'X-Viber-Auth-Token': process.env.VIBER_AUTH_TOKEN,
    }
  })
  // console.log(response)
  console.log("Chat bot connected: "+(response.data.status === 0))
  }catch(e){
      console.error(e);
      process.exit(1);
  }
// await bot.setWebhook(`${NGROK_URL}/viber/webhook`).catch(error => {
  // });
}

temFunc()


app.listen(port, async () => {
  console.log(`Application running on port: ${port}`);
});

// ngrok http --domain=workable-wildcat-eagerly.ngrok-free.app 80