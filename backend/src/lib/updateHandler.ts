// import { Register } from '../models/user_details';

import { Register } from "../models/Register";


// update the actual arrival time
export const updateSchedule = async (req, res) => {
  try {
    let { viberId, actual_arrival } = req.query;
    viberId = decodeURIComponent(viberId);
    actual_arrival = decodeURIComponent(actual_arrival);
    if(parseInt(actual_arrival) !== -1){

    const updatedUser = await Register.findOneAndUpdate(
      {
        viberId,
        actual_arrival: ""
      },
      {
        actual_arrival,
        bot_state: 0
      },
      { sort: { created_at: -1 }, new: true }
    ).exec();

    if (updatedUser) {
      if (updatedUser.actual_arrival > updatedUser.scheduled_arrival) {
        await Register.findOneAndUpdate(
          {
            viberId,
            actual_arrival
          },
          {
            status: "late",
            waitingForReason: true
          }
        );
      }
      else {
        await Register.findOneAndUpdate(
          {
            viberId: viberId,
            actual_arrival: actual_arrival
          },
          {
            status: "onschedule"
          }
        );
      }
      res.sendStatus(200);
    } else {
      res.sendStatus(404);
    }
  }else{
    const updatedUser = await Register.findOneAndUpdate(
      {
        viberId,
        actual_arrival: ""
      },
      {
        actual_arrival,
        bot_state: 0,
        status:"canceled"
      },
      { sort: { created_at: -1 }, new: true }
    ).exec();

      res.sendStatus(200);

  }
  
  } catch (error) {
    console.error(error);
    res.sendStatus(500);
  }
};