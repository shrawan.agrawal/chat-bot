// import { Register } from '../models/user_details';

import { Register } from "../models/Register";


/// registers the user details
export const handleRegister = async (req, res) => {
  try {

    let { viberId, name, department, from, to, scheduledArrivalTime, trip_time } = req.query;
    viberId = decodeURIComponent(viberId);
    name = decodeURIComponent(name);
    department = decodeURIComponent(department);
    from = decodeURIComponent(from);
    to = decodeURIComponent(to);
    scheduledArrivalTime = decodeURIComponent(scheduledArrivalTime);
    trip_time = decodeURIComponent(trip_time);
    const scheduled_date = new Date();
    
const timeParts = scheduledArrivalTime.split(':');

if (timeParts.length === 2) {
  const hours = parseInt(timeParts[0]);
  const minutes = parseInt(timeParts[1]);

  if (!isNaN(hours) && !isNaN(minutes)) {
    scheduled_date.setHours(hours);
    scheduled_date.setMinutes(minutes);
  }
} else {
  console.log("Invalid scheduledArrivalTime format");
}
    const newUser = new Register({
      viberId: viberId,
      name: name,
      department: department,
      from: from,
      to: to,
      scheduled_arrival: scheduled_date.getTime(),
      trip_time: trip_time,
      created_at: Date.now(),
      status: "",
      actual_arrival: "",
      bot_state: 1,
      waitingForReason: false
    });
    await newUser.save();    
    res.sendStatus(200);
  } catch (error) {
    console.error(error);
    res.sendStatus(500);
  }
}
