/* eslint-disable @typescript-eslint/no-unused-vars */

import dotenv from 'dotenv';
import request from "request";

import { Subscriber } from '../models/Subscriber';
import { Register } from '../models/Register';
import { Admin } from '../models/Admin';
import { UserDetail } from '../models/UserDetails';
import { CustomButtons } from '../models/CustomButtons';
// import axios, { AxiosRequestConfig } from 'axios';
import { MyKeyboard, Text, send } from './BotResponse';
dotenv.config();

const RegisterKeyboard = {
    Type: "keyboard",
    "InputFieldState": "hidden",
    Buttons: [{
        "Silent": false,
        ActionType: "reply",
        ActionBody: "register",
        Text: "Register new movement",
        TextSize: "large",
        BgColor: "#F5F5F5",
        TextColor: "#000000",
        BgMediaType: "picture",
        Frame: {
            BorderColor: '#808080',
            BorderWidth: 3,
            CornerRadius: 4,
        },
        BgMedia: "https://i.imgur.com/BB5G0wG.jpeg"
    }]
};

const CancelKeyboard = {
    Type: "keyboard",
    "InputFieldState": "hidden",
    Buttons: [{
        "Silent": false,
        ActionType: "reply",
        ActionBody: "cancel",
        Text: "cancel",
        TextSize: "large",
        BgColor: "#F5F5F5",
        TextColor: "#000000",
        BgMediaType: "picture",
        Frame: {
            BorderColor: '#808080',
            BorderWidth: 3,
            CornerRadius: 4,
        },
        BgMedia: "https://i.imgur.com/BB5G0wG.jpeg"
    }]
};
const abortKeyboard={
    Type: "keyboard",
    // "InputFieldState": "hidden",
    Buttons: [{
        "Silent": false,
        ActionType: "reply",
        ActionBody: "cancel",
        Text: "Abort",
        TextSize: "large",
        BgColor: "#F5F5F5",
        TextColor: "#000000",
        BgMediaType: "picture",
        Frame: {
            BorderColor: '#808080',
            BorderWidth: 3,
            CornerRadius: 4,
        },
        BgMedia: "https://i.imgur.com/BB5G0wG.jpeg"
    }]
};

export const handleIncomingMessage = async (message, sender) => {
    try {
        // console.log(sender.name)
        const NGROK_URL = process.env.NGROK_URL;
        let text = message.text.toLowerCase();
        const senderDoc = await Register.findOne({ viberId: sender.id }).sort({ created_at: -1 }).exec();

        const encodedId = encodeURIComponent(sender.id);
        const encodedName = encodeURIComponent(sender.name);

        // let dept = ['goss', 'IES', 'SAP', 'HR', 'R&D', 'OtherDept', 'Cancel'];
        let fromLoc = ['Home', 'RCH', 'From Other Loc', 'Cancel'];
        let toLoc = ['Home', 'RCH', 'To Other Loc', 'Cancel'];
        const minutes = ['30', '60', '90', '120', '150', 'Cancel'];

        let userDetail = await UserDetail.findOne({ viberId: sender.id }); 
        let subscriber = await Subscriber.findOne({ viberId: sender.id });
        let dept;
        if(subscriber && subscriber.state === "completed"){
            dept = subscriber.department
        }else{
            if(!subscriber){
                subscriber = new Subscriber({viberId:sender.id, name:'', department:'', state:"name_input"})
                await subscriber.save()
                await send(sender.id, "Can not find the user, please enter your name first.")
                return;
            }else if(text=="cancel" && subscriber.name!="" && subscriber.department!=''){
                subscriber.state="completed"
                await subscriber.save()
            }else if(subscriber.state === 'name'){
                if(subscriber.name=='')
                await send(sender.id, "Please input your name to register yourself.")
                else await send(sender.id, "Please input your name to register yourself.", abortKeyboard)
                subscriber.state = "name_input"
                await subscriber.save()
                return;
            }else if(subscriber.state === 'name_input'){
                subscriber.name = text
                subscriber.state = "department_input"
                await subscriber.save()
                await send(sender.id, "Please enter your department.", abortKeyboard)
                return;
            }else if(subscriber.state === 'department'){
                subscriber.state = "department_input"
                await subscriber.save()
                await send(sender.id, "Please enter your department.", abortKeyboard)
                return;
            }else if(subscriber.state === 'department_input') {
                subscriber.department = text.toUpperCase()
                subscriber.state="completed"
                dept = text.toUpperCase()
                await subscriber.save()
                text = "cancel"
                await send(sender.id, "Your details updated successfully!\nTo initiate next step please press cancel.",CancelKeyboard)
                return
            }
        }

        const userExists = await CustomButtons.findOne({ viberId: sender.id }).exec();

        // looking for the buttons
        if (userExists) {
            // dept = userExists.dept_list_rm;
            fromLoc = userExists.from_list_rm;
            toLoc = userExists.to_list_rm;

        } else {
            const newUserRM = new CustomButtons({
                viberId: sender.id
            });

            await newUserRM.save();
        }

        const fromlocation = fromLoc.map(locitem => ({
            "Silent": false,
            "Columns": 2,
            "Rows": 1,
            "ActionType":"reply",
            "ActionBody":locitem,
            "Text":`<font color="#777777" size="16"><b>${locitem}</b></font>`,
            "BgMediaType": "picture",
            "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
            "TextSize": "large",
            "TextVAlign": "middle",
            "TextHAlign": "middle",
            "BgColor": "#F5F5F5",
            "TextColor": "#8B4513",
            Frame: {
                BorderColor: '#808080',
                BorderWidth: 3,
                CornerRadius: 4,
            },
            }));
        const FROM_KEYBOARD = {
            "Type": "keyboard",
            "DefaultHeight": false,
            // BgColor:"#000055",
            "InputFieldState": "hidden",
            "Buttons": fromlocation
        };
// 
        const tolocation = toLoc.map(locitem => ({
            "Silent": false,
            "Columns": 2,
            "Rows": 1,
            "ActionType": "reply",
            "ActionBody": locitem,
            "Text": `<font color="#777777" size="16"><b>${locitem}</b></font>`,
            "BgMediaType": "picture",
            "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
            "TextSize": "large",
            "TextVAlign": "middle",
            "TextHAlign": "middle",
            "BgColor": "#F5F5F5",
            "TextColor": "#000000",
            Frame: {
                BorderColor: '#808080',
                BorderWidth: 3,
                CornerRadius: 4,
            },
        }));
        const TO_KEYBOARD = {
            "Type": "keyboard",
            "DefaultHeight": false,
            "InputFieldState": "hidden",
            "Buttons": tolocation
        };

        const mins = minutes.map(minitem => ({
            "Silent": false,
            "Columns": 2,
            "Rows": 1,
            "ActionType": "reply",
            "ActionBody": minitem,
            "Text": `<font color="#777777" size="16"><b>${minitem}</b></font>`,
            "BgMediaType": "picture",
            "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
            "TextSize": "large",
            "TextVAlign": "middle",
            "TextHAlign": "middle",
            "BgColor": "#F5F5F5",
            Frame: {
                BorderColor: '#808080',
                BorderWidth: 3,
                CornerRadius: 4,
            },
            "TextColor": "#00008B"
        }));
        const MINUTES_KEYBOARD = {
            "Type": "keyboard",
            "DefaultHeight": false,
            "InputFieldState": "hidden",
            "Buttons": mins
        };


        if (!userDetail) {
            //chng
            // console.log(subscriber)
            userDetail = new UserDetail({
                viberId: sender.id,
                name: subscriber.name,
                deptChoice: subscriber.department,
                fromChoice: null,
                toChoice: null,
                hourChoice: null,
                minuteChoice: null,
                userLate: 0,
                currentStage: '',
                nextExpectedInput: '',
                scheduledArrivalTimes: null,
                trip_time: ''
            });
            // console.log(userDetail)
            await userDetail.save();
        }

        // new data should register only when old data is complete or there is no old data
        if (senderDoc && senderDoc.waitingForReason === false && userDetail.currentStage === 'arrived' && !(text === 'cancel_schedule' || text === 'arrived')) {
            const Arrivedkeyboards = {
                "InputFieldState": "hidden",
                Type: "keyboard",
                Buttons: [{
                    "Silent": false,
                    "Columns": 3,
                    "Rows": 2,
                    ActionType: "reply",
                    ActionBody: "arrived",
                    Text: `<font color="#777777" size="16"><b>arrived</b></font>`,
                    TextSize: "large",
                    "BgMediaType": "picture",
                    "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                    "BgColor": "#F5F5F5",
                    Frame: {
                        BorderColor: '#808080',
                        BorderWidth: 3,
                        CornerRadius: 4,
                    },
                    "TextColor": "#00008B"
                },{
                    "Silent": false,
                    "Columns": 3,
                    "Rows": 2,
                    ActionType: "reply",
                    ActionBody: "cancel_schedule",
                    Text: `<font color="#777777" size="16"><b>Cancel Schedule</b></font>`,
                    TextSize: "large",
                    "BgMediaType": "picture",
                    "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                    "BgColor": "#F5F5F5",
                    Frame: {
                        BorderColor: '#808080',
                        BorderWidth: 3,
                        CornerRadius: 4,
                    },
                    "TextColor": "#00008B"
                }]
            };
            await send(sender.id, "Please check arrived when you reach the destination!", Arrivedkeyboards)
        }else if (text === 'register' && (senderDoc ? senderDoc.bot_state === 0 : true)) {
            userDetail.viberId= sender.id
            userDetail.name= subscriber.name
            userDetail.deptChoice= subscriber.department
            userDetail.fromChoice= null
            userDetail.toChoice= null
            userDetail.hourChoice= null
            userDetail.minuteChoice= null
            userDetail.userLate= 0
            userDetail.currentStage= ''
            userDetail.nextExpectedInput= ''
            userDetail.scheduledArrivalTimes= null
            userDetail.trip_time= ''
            await userDetail.save()
            await UserDetail.findOneAndUpdate(
                {
                    viberId: sender.id,
                },
                {
                    //chng
                    currentStage: 'choosing_from_loc',
                },
                { sort: { created_at: -1 }, new: true }
            )
            await send(sender.id,`Please click on your departure location, or select 'From Other Location' if it doesn't exist. Alternatively, you can choose 'Cancel' to abort the registration.`,FROM_KEYBOARD)
        }
        else if (text === 'cancel') {
            // userDetail.currentStage="previous_inputs"
            // userDetail.save()
            const senderDoc = await Register.findOne({ viberId: sender.id }).sort({ created_at: -1 }).exec();
            if (senderDoc ? !senderDoc.waitingForReason : false) {
                const N = 3;
                const latestNDocs = await Register.aggregate([
                    { $match: { viberId: senderDoc?.viberId } },
                    { $sort: { actual_arrival: -1 } },
                    {
                        //chng
                        $group: {
                            _id: { from: "$from", to: "$to", trip_time: "$trip_time" },
                            doc: { $first: "$$ROOT" }
                        }
                    },
                    { $limit: N }
                ]);
                const buttonsInitial = [
                    {    "Silent": false,
                        "Columns": 3,
                        "Rows": 2,
                        "ActionType": "reply",
                        "ActionBody": "register",
                        "Text": `<font color="#777777" size="16"><b>Register new movement</b></font>`,
                        "BgColor": "#F5F5F5",
                        "TextColor": "#000000",
                        "TextSize": "medium",
                        "BgMediaType": "picture",
                        "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                        Frame: {
                            BorderColor: '#808080',
                            BorderWidth: 3,
                            CornerRadius: 4,
                        },
                    },
                    {    "Silent": false,
                        "Columns": 3,
                        "Rows": 2,
                        "ActionType": "reply",
                        "ActionBody": "update_details",
                        "Text": `<font color="#777777" size="16"><b>Update personal details</b></font>`,
                        "BgColor": "#F5F5F5",
                        "TextSize": "medium",
                        "TextColor": "#000000",
                        "BgMediaType": "picture",
                        "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                        Frame: {
                            BorderColor: '#808080',
                            BorderWidth: 3,
                            CornerRadius: 4,
                        },
                    },
                ];

                let inpButton=[];
                latestNDocs.map((newGeneratedDoc) => {
                    const { from, to, trip_time, _id } = newGeneratedDoc.doc;
                    // console.log("previous_inputs for " + _id)
                    const buttonText = `Previous Input: \nLocation: ${from} -> ${to}\nETA: ${trip_time}`;
                    const docButton = {
                        "Silent": false,
                        "Columns": 6,
                        "Rows": 1,
                        "ActionType": "reply",
                        "ActionBody": "previous_inputs " + _id,
                        "Text": `<font color="#777777" size="16"><b>${buttonText}</b></font>`,
                        "BgColor": "#F5F5F5",
                        "TextColor": "#000000",
                        "TextSize": "medium",
                        "BgMediaType": "picture",
                        "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                        Frame: {
                            BorderColor: '#808080',
                            BorderWidth: 3,
                            CornerRadius: 4,
                        },
                    };

                    inpButton.push(docButton)
                })

                const buttonsFinal = inpButton.concat(buttonsInitial.sort());

                const buttonTemplate = {
                    Type: "keyboard",
                    "InputFieldState": "hidden",
                    Buttons: buttonsFinal
                };
                await UserDetail.findOneAndUpdate(
                    {
                        viberId: sender.id,
                    },
                    {
                        currentStage : 'previous_inputs'
    
                    },
                    { sort: { created_at: -1 }, new: true }
                ).exec();
                await send(sender.id,"To initiate a new registration, please enter 'Register New Movement,' or you can choose your previous input as the current option.", buttonTemplate);
                // await send(new TextMessage("Updated schedule successfully! \n Please enter 'register' for new registration or choose previous input as present input!", buttonTemplate,null,null,null, 6));
            }
            else{
                await send(sender.id,"Please enter 'register' for new registration!", RegisterKeyboard);

            }
        }
        else if(text === 'update_details') {
            userDetail.currentStage="previous_input"
            await userDetail.save()
            subscriber.state="name_input"
            await subscriber.save()
            await send(sender.id, "Please input your name to register yourself.", abortKeyboard)
        }

        else if (userDetail.currentStage === 'choosing_from_loc'&& userDetail.nextExpectedInput ==='' && fromLoc.some(f => text.includes(f.toLowerCase()))) {
            if (text.includes('from other loc')||text.includes('From Other Loc')) {
                await UserDetail.findOneAndUpdate(
                    {
                        viberId: sender.id,
                    },
                    {
                        nextExpectedInput: 'fromlocname',

                    },
                    { sort: { created_at: -1 }, new: true }
                );
                await send(sender.id,`Please enter the name of your alternate departure location.`);
            } else {
                await UserDetail.findOneAndUpdate(
                    {
                        viberId: sender.id,
                    },
                    {
                        currentStage: 'choosing_to_loc',
                        fromChoice: text
                    },
                    { sort: { created_at: -1 }, new: true }
                );
                await send(sender.id,`Please click on your destination location, or select 'To Other Location' if it doesn't exist. Alternatively, you can choose 'Cancel' to abort the registration process.`,TO_KEYBOARD);
            }
        }
        else if (userDetail.nextExpectedInput === 'fromlocname') {
            await UserDetail.findOneAndUpdate(
                {
                    viberId: sender.id,
                },
                {
                    fromChoice: text,
                    nextExpectedInput: ''
                },
                { sort: { created_at: -1 }, new: true }
            );
            const newFrom = text;

            const newFromRM = await CustomButtons.findOneAndUpdate({
                viberId: sender.id
            },
                {
                    $push: {
                        from_list_rm: {
                            $each: [newFrom],
                            $position: 0
                        }
                    }
                }
            );
            await UserDetail.findOneAndUpdate(
                {
                    viberId: sender.id,
                },
                {
                    currentStage: 'choosing_to_loc',

                },
                { sort: { created_at: -1 }, new: true }
            );
            await send(sender.id,`Please click on your destination location, or select 'To Other Location' if it doesn't exist. Alternatively, you can choose 'Cancel' to abandon the registration.`,TO_KEYBOARD);
        }
        else if (userDetail.currentStage === 'choosing_to_loc' && userDetail.nextExpectedInput==='' && toLoc.some(locitem => text.includes(locitem.toLowerCase()))) {
            if (text.includes('to other loc')||text.includes('To Other Loc')) {
                await UserDetail.findOneAndUpdate(
                    {
                        viberId: sender.id,
                    },
                    {
                        nextExpectedInput: 'tolocname',

                    },
                    { sort: { created_at: -1 }, new: true }
                );
                await send(sender.id,`Please enter the name of your destination location.`);
            } else {
                await UserDetail.findOneAndUpdate(
                    {
                        viberId: sender.id,
                    },
                    {
                        currentStage: 'choosing_trip_time',
                        toChoice: text

                    },
                    { sort: { created_at: -1 }, new: true }
                );
                await send(sender.id,`Please select the duration of your trip in minutes, or choose 'Cancel' to abort the registration.`,MINUTES_KEYBOARD);
            }
        }
        else if (userDetail.nextExpectedInput === 'tolocname') {

            await UserDetail.findOneAndUpdate(
                {
                    viberId: sender.id,
                },
                {
                    nextExpectedInput: '',
                    toChoice: text
                },
                { sort: { created_at: -1 }, new: true }
            );
            const newTo = text;
            const newToRM = await CustomButtons.findOneAndUpdate({
                viberId: sender.id
            },
                {
                    $push: {
                        to_list_rm: {
                            $each: [newTo],
                            $position: 0
                        }
                    }
                }
            );

            await UserDetail.findOneAndUpdate(
                {
                    viberId: sender.id,
                },
                {
                    currentStage: 'choosing_trip_time',

                },
                { sort: { created_at: -1 }, new: true }
            );
            await send(sender.id,`Please select the duration of your trip in minutes, or choose 'Cancel' to abort the registration.`,MINUTES_KEYBOARD);
        }

        else if (userDetail.currentStage === 'choosing_trip_time' && minutes.some(mn => text.includes(mn))) {
            await UserDetail.findOneAndUpdate(
                {
                    viberId: sender.id,
                },
                {
                    currentStage: 'arrived',
                    minuteChoice: text,

                },
                { sort: { created_at: -1 }, new: true }
            );
            const duration = text;
            let currentTime:any = Date.now();

            const targetTime = new Date(currentTime + duration * 60 * 1000);

            const hours = targetTime.getHours().toString().padStart(2, '0');
            const minutes = targetTime.getMinutes().toString().padStart(2, '0');
            const timeString = `${hours}:${minutes}`;
            const updated = await UserDetail.findOneAndUpdate(
                {
                    viberId: sender.id,
                },
                {
                    scheduledArrivalTimes: timeString,
                    trip_time: text,

                },
                { sort: { created_at: -1 }, new: true }
            ).exec();
            
            try {
                const encodedDept = encodeURIComponent(userDetail.deptChoice);
                const encodedFrom = encodeURIComponent(userDetail.fromChoice);
                const encodedTo = encodeURIComponent(userDetail.toChoice);
                const encodedScheduledArrivalTime = encodeURIComponent(updated?.scheduledArrivalTimes);
                const encodedTripTime = encodeURIComponent(updated?.trip_time);
                const options = {
                    url: `${NGROK_URL}/register?viberId=${encodedId}&name=${encodeURIComponent(subscriber.name)}&department=${encodeURIComponent(subscriber.department)}&from=${encodedFrom}&to=${encodedTo}&scheduledArrivalTime=${encodedScheduledArrivalTime}&trip_time=${encodedTripTime}`,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                };

                request(options, (error, response, body) => {
                    if (error) {
                        console.error(error);
                    } else {
                    }
                });
                const Arrivedkeyboards = {
                    "InputFieldState": "hidden",
                    Type: "keyboard",
                    Buttons: [{
                        "Silent": false,
                        "Columns": 3,
                        "Rows": 2,
                        ActionType: "reply",
                        ActionBody: "arrived",
                        Text: `<font color="#777777" size="16"><b>arrived</b></font>`,
                        TextSize: "large",
                        "BgMediaType": "picture",
                        "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                        "BgColor": "#F5F5F5",
                        Frame: {
                            BorderColor: '#808080',
                            BorderWidth: 3,
                            CornerRadius: 4,
                        },
                        "TextColor": "#00008B"
                    },{
                        "Silent": false,
                        "Columns": 3,
                        "Rows": 2,
                        ActionType: "reply",
                        ActionBody: "cancel_schedule",
                        Text: `<font color="#777777" size="16"><b>Cancel Schedule</b></font>`,
                        TextSize: "large",
                        "BgMediaType": "picture",
                        "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                        "BgColor": "#F5F5F5",
                        Frame: {
                            BorderColor: '#808080',
                            BorderWidth: 3,
                            CornerRadius: 4,
                        },
                        "TextColor": "#00008B"
                    }]
                };

                currentTime = new Date()
                currentTime.setMinutes(currentTime.getMinutes()+parseInt(updated.trip_time))
                await send(sender.id,`Schedule registered successfully!\n[Contact before moving]\n1) Date: ${currentTime.getFullYear()}/${currentTime.getMonth()+1}/${currentTime.getDate()}\n2) Group: ${subscriber.department}\n3) Name: ${subscriber.name}\n4) Location: ${updated.fromChoice} -> ${updated.toChoice}\n5) ETA: ${currentTime.getHours()}:${currentTime.getMinutes()}`, Arrivedkeyboards);

            } catch (error) {
                console.error(error);
            }


        }
        else if (userDetail.currentStage === 'arrived' && text === 'arrived') {

            const arrivedTime = Date.now();
            const encodedArrivedTime = encodeURIComponent(arrivedTime);
            try {
                const options = {
                    url: `${NGROK_URL}/update?viberId=${encodedId}&actual_arrival=${encodedArrivedTime}`,
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                };

                request(options, async (error, response, body) => {
                    if (error) {
                        console.error(error);
                    } else {
                        const senderDoc = await Register.findOne({ viberId: sender.id }).sort({ created_at: -1 }).exec();
                        if (senderDoc ? !senderDoc.waitingForReason : false) {
                            const N = 3;
                            const latestNDocs = await Register.aggregate([
                                { $match: { viberId: senderDoc?.viberId } },
                                { $sort: { actual_arrival: -1 } },
                                {
                                    $group: {
                                        _id: { department: "$department", from: "$from", to: "$to", trip_time: "$trip_time" },
                                        doc: { $first: "$$ROOT" }
                                    }
                                },
                                { $limit: N }
                            ]);

                            const buttonsInitial = [
                                    {    "Silent": false,
                                    "Columns": 3,
                                    "Rows": 2,
                                    "ActionType": "reply",
                                    "ActionBody": "register",
                                    "Text": `<font color="#777777" size="16"><b>Register new movement</b></font>`,
                                    "BgColor": "#F5F5F5",
                                    "TextColor": "#000000",
                                    "TextSize": "large",
                                    "BgMediaType": "picture",
                                    "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                                    Frame: {
                                        BorderColor: '#808080',
                                        BorderWidth: 3,
                                        CornerRadius: 4,
                                    },
                                },
                                {    "Silent": false,
                                    "Columns": 3,
                                    "Rows": 2,
                                    "ActionType": "reply",
                                    "ActionBody": "update_details",
                                    "Text": `<font color="#777777" size="16"><b>Update personal details</b></font>`,
                                    "BgColor": "#F5F5F5",
                                    "TextSize": "large",
                                    "TextColor": "#000000",
                                    "BgMediaType": "picture",
                                    Frame: {
                                        BorderColor: '#808080',
                                        BorderWidth: 3,
                                        CornerRadius: 4,
                                    },
                                    "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg"
                                },
                            ];
                            
                            let inpButton:any=[]
                            latestNDocs.map((newGeneratedDoc) => {
                                const { department, from, to, trip_time, _id} = newGeneratedDoc.doc;
                                const buttonText = `Previous Input: \nLocation: ${from} -> ${to}\nETA: ${trip_time}`;
                                const docButton = {
                                    "Silent": false,
                                    "Columns": 6,
                                    "Rows": 1,
                                    "ActionType": "reply",
                                    "ActionBody": "previous_inputs " + _id,
                                    "Text": `<font color="#777777" size="16"><b>${buttonText}</b></font>`,
                                    "BgColor": "#F5F5F5",
                                    "TextColor": "#000000",
                                    "TextSize": "medium",
                                    "BgMediaType": "picture",
                                    "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                                    Frame: {
                                        BorderColor: '#808080',
                                        BorderWidth: 3,
                                        CornerRadius: 4,
                                    },
                                };

                                inpButton.push(docButton)
                            })

                            const buttonsFinal = inpButton.concat(buttonsInitial.sort());

                            const buttonTemplate = {
                                Type: "keyboard",
                                "InputFieldState": "hidden",
                                Buttons: buttonsFinal
                            };
                            await UserDetail.findOneAndUpdate(
                                {
                                    viberId: sender.id,
                                },
                                {
                                    currentStage : 'previous_inputs' 
                
                                },
                                { sort: { created_at: -1 }, new: true }
                            );
                            await send(sender.id,"Schedule successfully updated! \nTo make a new registration, please enter 'Register new movement,' or you can choose your previous input as the current input.", buttonTemplate);
                        }
                        else {
                            await send(sender.id,"Apologies for the delay! Please provide the reason for your lateness.");

                            await UserDetail.findOneAndUpdate(
                                {
                                    viberId: sender.id,
                                },
                                {
                                    userLate: 1,

                                },
                                { sort: { created_at: -1 }, new: true }
                            ).exec();
                        }
                    }
                });
            } catch (error) {
                console.error(error);
            }
        }else if (userDetail.currentStage === 'arrived' && text === 'cancel_schedule') {
            const arrivedTime = Date.now();
            const encodedArrivedTime = encodeURIComponent(-1);
            try {
                const options = {
                    url: `${NGROK_URL}/update?viberId=${encodedId}&actual_arrival=${encodedArrivedTime}`,
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                };

                request(options, async (error, response, body) => {
                    if (error) {
                        console.error(error);
                    } else {
                        const senderDoc = await Register.findOne({ viberId: sender.id }).sort({ created_at: -1 }).exec();
                        if (senderDoc ? !senderDoc.waitingForReason : false) {
                            const N = 3;
                            const latestNDocs = await Register.aggregate([
                                { $match: { viberId: senderDoc?.viberId } },
                                { $sort: { actual_arrival: -1 } },
                                {
                                    $group: {
                                        _id: { department: "$department", from: "$from", to: "$to", trip_time: "$trip_time" },
                                        doc: { $first: "$$ROOT" }
                                    }
                                },
                                { $limit: N }
                            ]);

                            const buttonsInitial = [
                                    {    "Silent": false,
                                    "Columns": 3,
                                    "Rows": 2,
                                    "ActionType": "reply",
                                    "ActionBody": "register",
                                    "Text": `<font color="#777777" size="16"><b>Register new movement</b></font>`,
                                    "BgColor": "#F5F5F5",
                                    "TextColor": "#000000",
                                    "TextSize": "large",
                                    "BgMediaType": "picture",
                                    Frame: {
                                        BorderColor: '#808080',
                                        BorderWidth: 3,
                                        CornerRadius: 4,
                                    },
                                    "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                                },
                                {    "Silent": false,
                                    "Columns": 3,
                                    "Rows": 2,
                                    "ActionType": "reply",
                                    "ActionBody": "update_details",
                                    "Text": `<font color="#777777" size="16"><b>Update personal details</b></font>`,
                                    "BgColor": "#F5F5F5",
                                    "TextSize": "large",
                                    "TextColor": "#000000",
                                    "BgMediaType": "picture",
                                    Frame: {
                                        BorderColor: '#808080',
                                        BorderWidth: 3,
                                        CornerRadius: 4,
                                    },
                                    "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg"
                                },
                            ];
                            
                            let inpButton:any=[]
                            latestNDocs.map((newGeneratedDoc) => {
                                const { department, from, to, trip_time, _id} = newGeneratedDoc.doc;
                                const buttonText = `Previous Input: \nLocation: ${from} -> ${to}\nETA: ${trip_time}`;
                                const docButton = {
                                    "Silent": false,
                                    "Columns": 6,
                                    "Rows": 1,
                                    "ActionType": "reply",
                                    "ActionBody": "previous_inputs " + _id,
                                    "Text": `<font color="#777777" size="16"><b>${buttonText}</b></font>`,
                                    "BgColor": "#F5F5F5",
                                    "TextColor": "#000000",
                                    "TextSize": "medium",
                                    "BgMediaType": "picture",
                                    "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                                    Frame: {
                                        BorderColor: '#808080',
                                        BorderWidth: 3,
                                        CornerRadius: 4,
                                    },
                                };

                                inpButton.push(docButton)
                            })

                            const buttonsFinal = inpButton.concat(buttonsInitial.sort());

                            const buttonTemplate = {
                                Type: "keyboard",
                                "InputFieldState": "hidden",
                                Buttons: buttonsFinal
                            };
                            await UserDetail.findOneAndUpdate(
                                {
                                    viberId: sender.id,
                                },
                                {
                                    currentStage : 'previous_inputs' 
                
                                },
                                { sort: { created_at: -1 }, new: true }
                            );
                            await send(sender.id,"You schedule is cancelled!! \n Please enter 'Register new movement' for new registration or choose previous input as present input!", buttonTemplate);
                        }
                        else {
                            await send(sender.id,"You're late! Please enter reason for being late!");

                            await UserDetail.findOneAndUpdate(
                                {
                                    viberId: sender.id,
                                },
                                {
                                    userLate: 1,

                                },
                                { sort: { created_at: -1 }, new: true }
                            ).exec();
                        }
                    }
                });
            } catch (error) {
                console.error(error);
            }
        }
        else if (userDetail.currentStage === 'previous_inputs' && text.split(' ')[0] === 'previous_inputs') {
            await UserDetail.findOneAndUpdate(
                {
                    viberId: sender.id,
                },
                {
                    currentStage : 'arrived' 

                },
                { sort: { created_at: -1 }, new: true }
            ).exec();
            const senderDoc = await Register.findById(text.split(' ')[1])
            if(!senderDoc){
                await send(sender.id, "Please register againg, given input is more than a month old", CancelKeyboard)
                return;
            }
            const { from, to, trip_time} = senderDoc;
            const {department, name} = subscriber;
            let currentTime = new Date();
            const scheduled_arrival = new Date(currentTime.getTime() + parseInt(trip_time) * 60 * 1000).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });
            try {
                const encodedDept = encodeURIComponent(department);
                const encodedFrom = encodeURIComponent(from);
                const encodedTo = encodeURIComponent(to);
                const encodedScheduledArrival = encodeURIComponent(scheduled_arrival);
                const encodedTripTime = encodeURIComponent(trip_time);
                
                const options = {
                    url: `${NGROK_URL}/register?viberId=${encodedId}&name=${encodeURIComponent(name)}&department=${encodeURIComponent(department)}&from=${encodedFrom}&to=${encodedTo}&scheduledArrivalTime=${encodedScheduledArrival}&trip_time=${encodedTripTime}`,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                };

                request(options, (error, response, body) => {
                    if (error) {
                        console.error(error);
                    } else {
                    }
                });
                const Arrivedkeyboards = {
                    Type: "keyboard",
                    "InputFieldState": "hidden",
                    Buttons: [{
                        "Silent": false,
                        "Columns": 3,
                        "Rows": 2,
                        ActionType: "reply",
                        ActionBody: "arrived",
                        Text: `<font color="#777777" size="16"><b>arrived</b></font>`,
                        TextSize: "large",
                        "BgMediaType": "picture",
                        "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                        "BgColor": "#F5F5F5",
                        Frame: {
                            BorderColor: '#808080',
                            BorderWidth: 3,
                            CornerRadius: 4,
                        },
                        "TextColor": "#000000"
                    },{
                        "Silent": false,
                        "Columns": 3,
                        "Rows": 2,
                        ActionType: "reply",
                        ActionBody: "cancel_schedule",
                        Text: `<font color="#777777" size="16"><b>Cancel Schedule</b></font>`,
                        TextSize: "large",
                        "BgMediaType": "picture",
                        "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                        "BgColor": "#F5F5F5",
                        Frame: {
                            BorderColor: '#808080',
                            BorderWidth: 3,
                            CornerRadius: 4,
                        },
                        "TextColor": "#000000"
                    }]
                };
                currentTime.setMinutes(currentTime.getMinutes()+parseInt(trip_time))
                await send(sender.id,`[Contact before moving]\n1) Date: ${currentTime.getFullYear()}/${currentTime.getMonth()+1}/${currentTime.getDate()}\n2) Group: ${department}\n3) Name: ${name}\n4) Location: ${from} -> ${to}\n5) ETA: ${currentTime.getHours()}:${currentTime.getMinutes()}`, Arrivedkeyboards);
            } catch (error) {
                console.error(error);
            }
        }
        else if (senderDoc ? senderDoc.waitingForReason && userDetail.userLate : false) {

            await UserDetail.findOneAndUpdate(
                {
                    viberId: sender.id,
                },
                {
                    userLate: 0,

                },
                { sort: { created_at: -1 }, new: true }
            ).exec();
            await Register.findOneAndUpdate(
                {
                    viberId: sender.id
                },
                {
                    waitingForReason: false
                }
            );

            try {
                const admins = await Admin.find();
                // const message = new TextMessage(`User with ID ${sender.name} has given a reason for being late: ${text}`);
                await send(sender.id, "Please wait for few seconds!!")
                for (const admin of admins) {
                    const adminViberId = admin.viberId[0]; // Accessing the first viberId
                    const adminStatus = await UserDetail.findOne({viberId:admin.viberId[0]})
                    let keyboard
                    if(adminStatus.currentStage === "arrived"){
                        keyboard  = {
                          Type: "keyboard",
                          "InputFieldState": "hidden",
                          Buttons: [{
                              "Silent": false,
                              "Columns": 3,
                              "Rows": 2,
                              ActionType: "reply",
                              ActionBody: "arrived",
                              Text: `<font color="#777777" size="16"><b>arrived</b></font>`,
                              TextSize: "large",
                              "BgMediaType": "picture",
                              "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                              "BgColor": "#F5F5F5",
                              Frame: {
                                  BorderColor: '#808080',
                                  BorderWidth: 3,
                                  CornerRadius: 4,
                              },
                              "TextColor": "#000000"
                          },{
                              "Silent": false,
                              "Columns": 3,
                              "Rows": 2,
                              ActionType: "reply",
                              ActionBody: "cancel_schedule",
                              Text: `<font color="#777777" size="16"><b>Cancel Schedule</b></font>`,
                              TextSize: "large",
                              "BgMediaType": "picture",
                              "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                              "BgColor": "#F5F5F5",
                              Frame: {
                                  BorderColor: '#808080',
                                  BorderWidth: 3,
                                  CornerRadius: 4,
                              },
                              "TextColor": "#000000"
                          }]
                      }}else{
                        keyboard = {
                          Type: "keyboard",
                          "InputFieldState": "hidden",
                          Buttons: [{
                              "Silent": false,
                              ActionType: "reply",
                              ActionBody: "cancel",
                              Text: "Ok and go back",
                              TextSize: "large",
                              BgColor: "#F5F5F5",
                              TextColor: "#000000",
                              BgMediaType: "picture",
                              Frame: {
                                  BorderColor: '#808080',
                                  BorderWidth: 3,
                                  CornerRadius: 4,
                              },
                              BgMedia: "https://i.imgur.com/BB5G0wG.jpeg"
                          }]
                      };
                      }
                    await send(adminViberId,`User with name ${subscriber.name} has given a reason for being late: ${text}`, keyboard);
                }

                const RegisterKey = {
                    "Type": "keyboard",
                    "InputFieldState": "hidden",
                    "Buttons": [{
                        "Silent": false,
                        "ActionType": "reply",
                        "ActionBody": "register",
                        "Text": `<font color="#777777" size="16"><b>Register my movement</b></font>`,
                        "TextSize": "large",
                        "BgColor": "#F5F5F5",
                        "TextColor": "#000000",

                        "BgMediaType": "picture",
                        Frame: {
                            BorderColor: '#808080',
                            BorderWidth: 3,
                            CornerRadius: 4,
                        },
                        "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                    }]
                };
                const senderDoc = await Register.findOne({ viberId: sender.id }).sort({ created_at: -1 }).exec();
                if (senderDoc) {
                    const { department, from, to, trip_time } = senderDoc;

                    const buttonText = `Previous Inputs: \n Department: ${department} \n From: ${from} To: ${to}\nTrip Time: ${trip_time}`;

                    const buttonTemplate = {
                        Type: "keyboard",
                        "InputFieldState": "hidden",
                        Buttons: [
                            {    "Silent": false,
                                "ActionType": "reply",
                                "ActionBody": "previous_inputs",
                                "Text": `<font color="#777777" size="16"><b>${buttonText}</b></font>`,
                                "BgColor": "#F5F5F5",
                                "TextColor": "#000000",

                                "BgMediaType": "picture",
                                Frame: {
                                    BorderColor: '#808080',
                                    BorderWidth: 3,
                                    CornerRadius: 4,
                                },
                                "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                            },
                            {    "Silent": false,
                                "ActionType": "reply",
                                "ActionBody": "register",
                                "Text": `<font color="#777777" size="16"><b>Register my movement</b></font>`,
                                "TextSize": "large",
                                "BgColor": "#F5F5F5",
                                "TextColor": "#000000",

                                "BgMediaType": "picture",
                                Frame: {
                                    BorderColor: '#808080',
                                    BorderWidth: 3,
                                    CornerRadius: 4,
                                },
                                "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                            }
                        ]
                    };
                    await send(sender.id,"Schedule successfully updated! \nTo make a new registration, please enter 'register,' or you can choose your previous input as the current input.", buttonTemplate);
                }
                else {
                    await send(sender.id,"Updated schedule successfully! \nPlease enter 'register' for new registration!", RegisterKey);
                }
            }

            catch (error) {
                console.error(`Error sending message to admin: ${error.message}`);
            }

        }

        else if (text === 'name') {


            await send(sender.id,`Please type your name in English or Kanji`);


            await UserDetail.findOneAndUpdate(
                {
                    viberId: sender.id,
                },
                {
                    nextExpectedInput: text,

                },
                { sort: { created_at: -1 }, new: true }
            ).exec();

        }
        else if (userDetail.nextExpectedInput === 'name') {
            await UserDetail.findOneAndUpdate(
                {
                    viberId: sender.id,
                },
                {
                    name: text,
                    nextExpectedInput: ""

                },
                { sort: { created_at: -1 }, new: true }
            ).exec();
            const newUser = new Subscriber({
                viberId: sender.id,
                name: text
            });
            await newUser.save();
            const registerButton = {
                "Type": "keyboard",
                "InputFieldState": "hidden",
                "Buttons": [{
                    "Silent": false,
                    "ActionType": "reply",
                    "ActionBody": "register",
                    "Text": "Register my movement",
                    "TextSize": "large",
                    "BgColor": "#F5F5F5",
                    "TextColor": "#000000",
                    Frame: {
                        BorderColor: '#808080',
                        BorderWidth: 3,
                        CornerRadius: 4,
                    },

                    "BgMediaType": "picture",
                    "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",

                }]
            };
            // const registerKeyboardMessage = new KeyboardMessage(registerButton,null,null,null, 6);
            await send(sender.id,'',registerButton);
           
        }

        else {
            if (senderDoc ? senderDoc.bot_state === 1 : false)
                // need to  put the arrival keyboard
                await send(sender.id,"Please enter 'arrived' before registering a new schedule", RegisterKeyboard);
            else {
                await send(sender.id,"Please 'cancel' and re-register ", CancelKeyboard);
            }
        }
    } catch (error) {
        console.error('An error occurred:', error);
    }
}
