import axios, { AxiosRequestConfig } from "axios";
import { Keyboard, Message, RichMedia } from "./message";

const headers: AxiosRequestConfig['headers'] = {
    'X-Viber-Auth-Token': process.env.VIBER_AUTH_TOKEN,
};


export const send = async(id, message, keyboardArr?) => {
  // console.log(message)
  // console.log(process.env.viberApi+"send_message")
  const msg:Message = {
    "receiver":id,
    "min_api_version":7,
    "type":"text",
    "text":message,
    "keyboard":keyboardArr
 }
//  if(keyboardArr){
//     msg.keyboard={
//       "Type":"keyboard",
//       "DefaultHeight":false,
//       "Buttons":keyboardArr.map(dept => ({
//       "Silent": false,
//       "Columns": 1,
//       "Rows": 1,
//       "ActionType": "reply",
//       "ActionBody": dept,
//       "Text": `<font color="#323232">${dept}</font>`,
//       "BgMediaType": "picture",
//       "BgMedia": "https://i.imgur.com/FQOTzVg.png",
//       "BgColor": "#FFFFFF",
//       "TextColor": "#000000",
//       "TextSize": "small",
//       "TextVAlign": "middle",
//       "TextHAlign": "middle",
//     })),
//   }
//  }
  await axios.post(process.env.viberApi+"send_message",msg,{headers:{'X-Viber-Auth-Token': process.env.VIBER_AUTH_TOKEN,}})
}


export const Text = (
    userID: string,
    textMsg: string,
    keyboard?: Keyboard,
    richMedia?: RichMedia
  ): Message => {
    const message: Message = {
      receiver: userID,
      min_api_version: 7,
      type: 'text',
      text: textMsg,
    }
    if (keyboard) {
      message.keyboard = keyboard
    }
    if (richMedia) {
      message.rich_media = richMedia
    }
    return message
}

export const MyKeyboard = (
    userID: string,
    textMsg: string,
    keyboard?: Keyboard,
    richMedia?: RichMedia
  ): Message => {
    const message: Message = {
      receiver: userID,
      min_api_version: 7,
      type: 'text',
      text: textMsg,
    }
    if (keyboard) {
      message.keyboard = keyboard
    }
    if (richMedia) {
      message.rich_media = richMedia
    }
    return message
}
