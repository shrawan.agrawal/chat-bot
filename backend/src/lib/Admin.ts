import { Admin } from "../models/Admin";
import { CustomButtons } from "../models/CustomButtons";
import { Register } from "../models/Register";
import { UserDetail } from "../models/UserDetails";
import { send } from "./BotResponse";



export const admin = async() => {
    await Admin.findOne({ viberId: process.env.ADMIN_ID })
    .then(async(admin) => {
    if (!admin) { //admin not found
      const newAdmin = new Admin({
        viberId: [process.env.ADMIN_ID]
      });

      await newAdmin.save()
        .then(() => {})
        .catch((err: Error) => console.error(err));
    } else {
      //admin already exists
    }
  })
  .catch((err: Error) => console.error(err));
}

export const adminUpdate = async() => {
  console.log("admin update")
  if(new Date().getHours()>4 && new Date().getHours()<24){
    const late = await Register.find({ bot_state: 1, notified:false, scheduled_arrival: { $lt: Date.now() } });
    const admins = await Admin.find({})
    late.forEach(async(l)=>{
      admins.forEach(async(admin)=>{ 
        const adminStatus = await UserDetail.findOne({viberId:admin.viberId[0]})
        let keyboard
        if(adminStatus.currentStage === "arrived"){
          keyboard  = {
            Type: "keyboard",
            "InputFieldState": "hidden",
            Buttons: [{
                "Silent": false,
                "Columns": 3,
                "Rows": 2,
                ActionType: "reply",
                ActionBody: "arrived",
                Text: `<font color="#777777" size="16"><b>arrived</b></font>`,
                TextSize: "large",
                "BgMediaType": "picture",
                "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                "BgColor": "#F5F5F5",
                Frame: {
                    BorderColor: '#808080',
                    BorderWidth: 3,
                    CornerRadius: 4,
                },
                "TextColor": "#000000"
            },{
                "Silent": false,
                "Columns": 3,
                "Rows": 2,
                ActionType: "reply",
                ActionBody: "cancel_schedule",
                Text: `<font color="#777777" size="16"><b>Cancel Schedule</b></font>`,
                TextSize: "large",
                "BgMediaType": "picture",
                "BgMedia": "https://i.imgur.com/BB5G0wG.jpeg",
                "BgColor": "#F5F5F5",
                Frame: {
                    BorderColor: '#808080',
                    BorderWidth: 3,
                    CornerRadius: 4,
                },
                "TextColor": "#000000"
            }]
        }}else{
          keyboard = {
            Type: "keyboard",
            "InputFieldState": "hidden",
            Buttons: [{
                "Silent": false,
                ActionType: "reply",
                ActionBody: "cancel",
                Text: "Ok and go back",
                TextSize: "large",
                BgColor: "#F5F5F5",
                TextColor: "#000000",
                BgMediaType: "picture",
                Frame: {
                    BorderColor: '#808080',
                    BorderWidth: 3,
                    CornerRadius: 4,
                },
                BgMedia: "https://i.imgur.com/BB5G0wG.jpeg"
            }]
        };
        }
        await send(admin.viberId[0], `${l.name} is late. Please check with him.\n1) Group: ${l.department}
2) Location: ${l.from} -> ${l.to}
3) ETA: ${new Date(l.scheduled_arrival).toLocaleTimeString().split(' ')[0].slice(0,-3)+' '+ new Date(l.scheduled_arrival).toLocaleTimeString().split(' ')[1]}`,keyboard)
      })
      l.notified=true
      l.save()
    })
    // console.log(Date.now(), late)
  }
}