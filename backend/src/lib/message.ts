export interface Message {
  receiver: string
  type: string
  min_api_version: number
  text?: string
  rich_media?: RichMedia
  keyboard?: any
}

export interface RichMedia {
  Type: 'rich_media'
  Buttons: Array<CustomButton>
  ButtonsGroupColumns: number
  ButtonsGroupRows: number
  BgColor: string
}

export interface Keyboard {
  DefaultHeight: boolean
  InputFieldState: string
  BgColor?: string
  Buttons: Array<CustomButton>
}

export interface CustomButton {
  Columns?: number
  Rows?: number
  ActionType: 'open-url' | 'reply'
  ActionBody?: string
  Image?: string
  Silent?: boolean
  BgColor?: string
  Text?: string
  TextSize?: string
  TextVAlign?: string
  TextHAlign?: string
  TextOpacity?: number
  OpenURLType?: string
  OpenURLMediaType?: 'not-media'

  InternalBrowser?: any
  Frame?: {
    CornerRadius?: number
    BorderColor?: string
    BorderWidth?: number
  }
}
