import mongoose from 'mongoose';

export const connect = async() => {

    try {
        const mongoUrl = process.env.MONGO_URL;
        
        if (!mongoUrl) {
            throw new Error("The MONGO_URL environment variable is not defined!");
        }
    // mongoose.connect(mongoUrl);
    // process.setMaxListeners(20);
        await mongoose.connect(mongoUrl);
        console.log('Connected to MongoDB');
      } catch (error) {
        console.error('Error connecting to MongoDB', error);
        process.exit(1);
    }
}